# g2g

Develop Essential Features enabling inter-operations between Semantic Graph and Property Graph.

Draft

Features includ but not limit to:
1.	Data/Instances conversion
1.1 Data in semantic graph format converts into the data in property graph format.
1.2 Data in property graph format converts into the data in semantic graph format.

2.	Models/Schema conversion
2.1 Convert Ontology into property graph model
2.2 Convert proerty graph model into ontology

3.	CRUD APIs
3.1 Tranlate a unit property graph CRUD into semantic graph operations. 
3.2 Tranlate a unit semantic graph CRUD into property graph operations. 

4.	Graph Visualization (To be defined)

5.	Functionalities/Algorithms
Translate popular, general purpose functions implemented in property graph to semantic graph algorithms, vice versa.

6.	KB querying
Property Graph Query functions and SPARQL statements 

7.	Search/Indexing (To be defined)